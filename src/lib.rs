// Copyright 2020 Developers of the Rand project.
// Copyright 2017-2020 The Rust Project Developers.
//
// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// https://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or https://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

//! 提供了一个随机产生身份证号的工具或者验证方法
//!
//! 可以随机产生身份证号或者指定地区，生日来产生身份证号
//! 
//! 身份证号码的生成标准是根据1999年中国国家质量技术监督局实施的GB11643-1999《公民身份号码》来实现的,
//! 具体请参照[百度百科](https://baike.baidu.com/item/%E4%B8%AD%E5%8D%8E%E4%BA%BA%E6%B0%91%E5%85%B1%E5%92%8C%E5%9B%BD%E5%B1%85%E6%B0%91%E8%BA%AB%E4%BB%BD%E8%AF%81/4471408?fromtitle=%E5%B1%85%E6%B0%91%E8%BA%AB%E4%BB%BD%E8%AF%81&fromid=2080960)
//!
//! 行政区划代码根据[中华人民共和国民政部官网](http://www.mca.gov.cn)提供的[2020年7月中华人民共和国县以上行政区划代码](http://www.mca.gov.cn//article/sj/xzqh/2020/2020/20200908007001.html)生成的行政区划代码

pub mod codes;
pub mod ic;
pub mod random;

pub use codes::{get_loc_code, LOC_CODES, REGION_CODES, SIGN_CODES, VERIFY_CODES};
pub use ic::{compute_verify_code, IdentityCard};
pub use random::generate_identitycard;

#[cfg(test)]
mod tests {

    use crate::{generate_identitycard, IdentityCard};
    use std::str::FromStr;

    #[test]
    fn test_random() {
        let ic = generate_identitycard("610102".to_string(), "19910201".to_string());
        assert_eq!(ic.to_string().starts_with("610102"), true);
        assert_eq!(ic.to_string().starts_with("61010219910201"), true);
        assert_eq!(ic.verify_ic(), true);
    }

    #[test]
    fn test_fromstr() {
        let ic_number = "440222199602175492";
        let ic = IdentityCard::from_str(ic_number).unwrap();
        println!("{:#?}\n{}", ic, ic);
        assert_eq!(ic.to_string(), ic_number.to_string());
        assert_eq!(ic.verify_ic(), true);
    }

    #[test]
    fn test_leap() {
        let ic = generate_identitycard("西安市".to_string(), "202002".to_string());
        println!("{:#?}", ic);
        assert_eq!(ic.verify_ic(), true)
    }
}
